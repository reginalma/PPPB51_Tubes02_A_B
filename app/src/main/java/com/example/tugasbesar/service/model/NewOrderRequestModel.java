package com.example.tugasbesar.service.model;

public class NewOrderRequestModel {
    public String course_id;
    public String seats;

    public NewOrderRequestModel(String course_id, String seats) {
        this.course_id = course_id;
        this.seats = seats;
    }
}
