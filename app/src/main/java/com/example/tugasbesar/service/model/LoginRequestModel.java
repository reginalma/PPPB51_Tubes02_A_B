package com.example.tugasbesar.service.model;

public class LoginRequestModel {
    int id;
    String username;
    String password;

    public LoginRequestModel(String username, String password){
        this.username=username;
        this.password=password;
    }

    public void setId(int id){
        this.id=id;
    }

    public int getId(){
        return id;
    }

    public void setUsername(String username){
        this.username=username;
    }

    public String getUsername(){
        return username;
    }

    public void setPassword(String password){
        this.password=password;
    }

    public String getPassword(){
        return password;
    }
}
