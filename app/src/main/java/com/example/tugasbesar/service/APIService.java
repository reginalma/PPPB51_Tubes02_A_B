package com.example.tugasbesar.service;

import android.content.Context;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.tugasbesar.service.model.CourseRequestModel;
import com.example.tugasbesar.service.model.NewOrderRequestModel;
import com.example.tugasbesar.service.model.LoginRequestModel;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class APIService {
    public static final String URL = "https://devel.loconode.com/pppb/v1/";
    private final Gson gson;
    private final Context context;

    public APIService(Context context){
        gson = new Gson();
        this.context = context;
    }

    public void loginTask(LoginRequestModel userInput, APICallback apiCallback) {
        String body = gson.toJson(userInput);
        try {
            JsonObjectRequest jsonRequest = new JsonObjectRequest(
                    Request.Method.POST,
                    URL + "authenticate",
                    new JSONObject(body),
                    apiCallback::onSuccess,
                    apiCallback::onFail
            ){
                @Override
                public String getBodyContentType(){
                    return "application/json";
                }
            };
            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsonRequest);
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context, "Gagal login", Toast.LENGTH_SHORT).show();
        }
    }

    public void routeTask(String token, APICallback apiCallback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                URL + "routes",
                null,
                apiCallback::onSuccess,
                apiCallback::onFail
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }

    public void courseTask(String token, CourseRequestModel course, APICallback apiCallback) {
        String url = String.format("%scourses/?source=%s&destination=%s&vehicle=%s&date=%s&hour=%s", URL, course.source, course.destination, course.vehicle, course.date, course.hour);
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                apiCallback::onSuccess,
                apiCallback::onFail
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }

    public void newOrderTask(String token, NewOrderRequestModel requestModel, APICallback apiCallback) {
        String body = gson.toJson(requestModel);
        try {
            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.POST,
                    URL + "orders",
                    new JSONObject(body),
                    apiCallback::onSuccess,
                    apiCallback::onFail
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + token);
                    return headers;
                }
            };
            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(request);
        } catch (JSONException e){
            e.printStackTrace();
            Toast.makeText(context, "Gagal melakukan order", Toast.LENGTH_SHORT).show();
        }
    }

    public void historyTask(String token, APICallback apiCallback) {
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                URL + "orders?limit=10&offset=0",
                null,
                apiCallback::onSuccess,
                apiCallback::onFail
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }
}

