package com.example.tugasbesar.service.model;

public class CourseRequestModel {
    public String source;
    public String destination;
    public String vehicle;
    public String date;
    public String hour;

    public CourseRequestModel(String source, String destination, String vehicle, String date, String hour) {
        this.source = source;
        this.destination = destination;
        this.vehicle = vehicle;
        this.date = date;
        this.hour = hour;
    }
}
