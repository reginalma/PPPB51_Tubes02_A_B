package com.example.tugasbesar.service;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface APICallback {
    void onSuccess(JSONObject response);
    void onFail(VolleyError error);
}
