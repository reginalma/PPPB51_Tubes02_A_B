package com.example.tugasbesar.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.example.tugasbesar.R;
import com.example.tugasbesar.view.history.History;

import java.util.ArrayList;
import java.util.List;

public class PemesananAdapter extends BaseAdapter{
    private List<History> list;
    private Fragment fragment;

    public PemesananAdapter(Fragment fragment){
        this.fragment = fragment;
        this.list = new ArrayList<>();
    }

    public void add(List<History> list){
        this.list = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//        Fragment fragment = this.fragment;
//        View itemView = this.fragment.getLayoutInflater().inflate(R.layout.list_pemesanan, null);
//        TextView tv = itemView.findViewById(R.id.lst_movies);
//        tv.setText(this.list.get(position));

//        return itemView;
        if (convertView == null) {
            convertView = LayoutInflater.from(fragment.getActivity()).
                    inflate(R.layout.list_pemesanan, parent, false);
        }

        History history = (History) getItem(position);

        TextView tvSource = (TextView) convertView.findViewById(R.id.tv_kota_asal2);
        TextView tvDestination = (TextView) convertView.findViewById(R.id.tv_kota_tujuan2);
        TextView tvDate = (TextView) convertView.findViewById(R.id.tv_tanggal2);

        tvSource.setText(history.source);
        tvDestination.setText(history.destination);
        tvDate.setText(history.course_datetime);

        return convertView;
    }
}
