package com.example.tugasbesar.view.seat;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tugasbesar.R;
import com.example.tugasbesar.databinding.SeatLargeBinding;
import com.example.tugasbesar.service.model.CourseRequestModel;
import com.example.tugasbesar.view.order.OrderViewModel;

import java.util.Arrays;

public class SeatLargeFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {
    SeatLargeBinding binding;
    GestureDetector mDetector;
    Bitmap mBitmap;
    Canvas mCanvas;
    Paint seatKosong;
    Paint seatDipilih;
    Paint strokeSeatKosong;
    Paint seatText;
    Paint strokeSeatDipilih;
    int[] ditekan=new int[10];
    private SeatViewModel viewModel;

    public static SeatLargeFragment newInstance(){
        SeatLargeFragment fragment = new SeatLargeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = SeatLargeBinding.inflate(inflater, container, false);
        this.startCanvas();
        this.mDetector = new GestureDetector(this.getContext(), new customGestureListener());
        this.binding.ivSeat.setOnTouchListener(this);
        this.binding.btnLanjut.setOnClickListener(this);
        return this.binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.viewModel = new ViewModelProvider(this).get(SeatViewModel.class);
//        CourseRequestModel diisi dari form pesan
        this.viewModel.getCourse(new CourseRequestModel("", "", "", "", ""));
        this.viewModel.takenSeats.observe(requireActivity(), seat -> Toast.makeText(getActivity(), "Taken seat + " + Arrays.toString(seat.seats), Toast.LENGTH_SHORT).show());
    }

    @Override
    public void onClick(View v) {
        Log.d("debug", "clicked!");
        if (v == this.binding.btnLanjut) {
            Bundle result = new Bundle();
            result.putInt("page", 5);
            this.getParentFragmentManager().setFragmentResult("changePage", result);
        }
    }

    public void startCanvas(){
        //Create bitmap
        this.mBitmap=Bitmap.createBitmap(350,500,Bitmap.Config.ARGB_8888);

        //Associate the bitmap to the ImageView
        this.binding.ivSeat.setImageBitmap(mBitmap);

        //Create a canvas with the bitmap
        //Inisialisasi dengan bitmap
        this.mCanvas=new Canvas(mBitmap);

        //Set canvas background
        int mColorBackground= ResourcesCompat.getColor(getResources(),
                R.color.white, null);
        mCanvas.drawColor(mColorBackground);

        //Color and style
        //SeatLargeFragment kosong
        this.seatKosong = new Paint();
        seatKosong.setColor(this.getResources().getColor(R.color.black));
        this.seatKosong.setStyle(Paint.Style.FILL);

        this.strokeSeatKosong = new Paint();
        strokeSeatKosong.setColor(this.getResources().getColor(R.color.gold));
        this.seatKosong.setStyle(Paint.Style.STROKE);
        this.strokeSeatKosong.setStrokeWidth(5);

        this.seatText = new Paint();
        seatText.setColor(this.getResources().getColor(R.color.black));
        this.seatText.setTextSize(20);

        //SeatLargeFragment dipilih
        seatDipilih = new Paint();
        seatDipilih.setColor(getResources().getColor(R.color.black));
        seatDipilih.setStyle(Paint.Style.FILL);

        strokeSeatDipilih = new Paint();
        strokeSeatDipilih.setColor(this.getResources().getColor(R.color.red));
        seatDipilih.setStyle(Paint.Style.STROKE);
        strokeSeatDipilih.setStrokeWidth(5);

        seatText = new Paint();
        seatText.setColor(this.getResources().getColor(R.color.black));
        seatText.setTextSize(20);

        //SeatLargeFragment nomor 1
        mCanvas.drawRect(60,60,110,110,seatKosong);
        mCanvas.drawRect(61,61,109,109,strokeSeatKosong);
        mCanvas.drawText("1",80,90,seatText);

        //SeatLargeFragment supir
        mCanvas.drawRect(240,60,290,110,seatKosong);
        mCanvas.drawRect(241,61,289,109,strokeSeatKosong);
        mCanvas.drawText("S",260,90,seatText);

        //SeatLargeFragment nomor 2
        mCanvas.drawRect(150,150,200,200,seatKosong);
        mCanvas.drawRect(151,151,199,199,strokeSeatKosong);
        mCanvas.drawText("2",170,180,seatText);

        //SeatLargeFragment nomor 3
        mCanvas.drawRect(240,150,290,200,seatKosong);
        mCanvas.drawRect(241,151,289,199,strokeSeatKosong);
        mCanvas.drawText("3",260,180,seatText);

        //SeatLargeFragment nomor 4
        mCanvas.drawRect(60,240,110,290,seatKosong);
        mCanvas.drawRect(61,241,109,289,strokeSeatKosong);
        mCanvas.drawText("4",80,270,seatText);

        //SeatLargeFragment nomor 5
        mCanvas.drawRect(240,240,290,290,seatKosong);
        mCanvas.drawRect(241,241,289,289,strokeSeatKosong);
        mCanvas.drawText("5",260,270,seatText);

        //SeatLargeFragment nomor 6
        mCanvas.drawRect(60,330,110,380,seatKosong);
        mCanvas.drawRect(61,331,109,379,strokeSeatKosong);
        mCanvas.drawText("6",80,360,seatText);

        //SeatLargeFragment nomor 7
        mCanvas.drawRect(240,330,290,380,seatKosong);
        mCanvas.drawRect(241,331,289,379,strokeSeatKosong);
        mCanvas.drawText("7",260,360,seatText);

        //SeatLargeFragment nomor 8
        mCanvas.drawRect(60,420,110,470,seatKosong);
        mCanvas.drawRect(61,421,109,469,strokeSeatKosong);
        mCanvas.drawText("8",80,450,seatText);

        //SeatLargeFragment nomor 9
        mCanvas.drawRect(150,420,200,470,seatKosong);
        mCanvas.drawRect(151,421,199,469,strokeSeatKosong);
        mCanvas.drawText("9",170,450,seatText);

        //SeatLargeFragment nomor 10
        mCanvas.drawRect(240,420,290,470,seatKosong);
        mCanvas.drawRect(241,421,289,469,strokeSeatKosong);
        mCanvas.drawText("10",254,450,seatText);
        Log.d("test","test");
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.mDetector.onTouchEvent(motionEvent);
        return true;
    }

    private class customGestureListener extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onDown(MotionEvent motionEvent){
            Log.d("X=",String.valueOf(motionEvent.getX()));
            Log.d("Y=",String.valueOf(motionEvent.getY()));

            float x = motionEvent.getX();
            float y = motionEvent.getY();

            //Seat nomor 1
            if(x >= 150 && x <= 190 && y >= 150 && y <= 190){
                if(ditekan[0]==0){
                    mCanvas.drawRect(60,60,110,110,seatDipilih);
                    mCanvas.drawRect(61,61,109,109,strokeSeatDipilih);
                    mCanvas.drawText("1",80,90,seatText);
                    ditekan[0]=1;
                }else{
                    mCanvas.drawRect(60,60,110,110,seatKosong);
                    mCanvas.drawRect(61,61,109,109,strokeSeatKosong);
                    mCanvas.drawText("1",80,90,seatText);                    
                    ditekan[0]=0;
                }
            }
            //Seat nomor 2
            else if(x >= 300 && x <= 350 && y >= 300 && y <= 350){
                if(ditekan[1]==0){
                    mCanvas.drawRect(150,150,200,200,seatDipilih);
                    mCanvas.drawRect(151,151,199,199,strokeSeatDipilih);
                    mCanvas.drawText("2",170,180,seatText);
                    ditekan[1]=1;
                }else{
                    mCanvas.drawRect(150,150,200,200,seatKosong);
                    mCanvas.drawRect(151,151,199,199,strokeSeatKosong);
                    mCanvas.drawText("2",170,180,seatText);                    
                    ditekan[1]=0;
                }
            }
            //Seat nomor 3
            else if(x >= 480 && x <= 530 && y >= 290 && y <= 390){
                if(ditekan[2]==0){
                    mCanvas.drawRect(240,150,290,200,seatDipilih);
                    mCanvas.drawRect(241,151,289,199,strokeSeatDipilih);
                    mCanvas.drawText("3",260,180,seatText);
                    ditekan[2]=1;
                }else{
                    mCanvas.drawRect(240,150,290,200,seatKosong);
                    mCanvas.drawRect(241,151,289,199,strokeSeatKosong);
                    mCanvas.drawText("3",260,180,seatText);
                    ditekan[2]=0;
                }
            }
            //Seat nomor 4
            else if(x >= 140 && x <= 180 && y >= 490 && y <= 550){
                if(ditekan[3]==0){
                    mCanvas.drawRect(60,240,110,290,seatDipilih);
                    mCanvas.drawRect(60,240,110,290,strokeSeatDipilih);
                    mCanvas.drawText("4",80,270,seatText);
                    ditekan[3]=1;
                }else{
                    mCanvas.drawRect(60,240,110,290,seatKosong);
                    mCanvas.drawRect(60,240,110,290,strokeSeatKosong);
                    mCanvas.drawText("4",80,270,seatText);
                    ditekan[3]=0;
                }
            }
            //Seat nomor 5
            else if(x >= 490 && x <= 550 && y >= 490 && y <= 560){
                if(ditekan[4]==0){
                    mCanvas.drawRect(240,240,290,290,seatDipilih);
                    mCanvas.drawRect(241,241,289,289,strokeSeatDipilih);
                    mCanvas.drawText("5",260,270,seatText);
                    ditekan[4]=1;
                }else{
                    mCanvas.drawRect(240,240,290,290,seatKosong);
                    mCanvas.drawRect(241,241,289,289,strokeSeatKosong);
                    mCanvas.drawText("5",260,270,seatText);
                    ditekan[4]=0;
                }
            }
            //Seat nomor 6
            else if(x >= 130 && x <= 180 && y >= 650 && y <= 720){
                if(ditekan[5]==0){
                    mCanvas.drawRect(60,330,110,380,seatDipilih);
                    mCanvas.drawRect(61,331,109,379,strokeSeatDipilih);
                    mCanvas.drawText("6",80,360,seatText);
                    ditekan[5]=1;
                }else{
                    mCanvas.drawRect(60,330,110,380,seatKosong);
                    mCanvas.drawRect(61,331,109,379,strokeSeatKosong);
                    mCanvas.drawText("6",80,360,seatText);
                    ditekan[5]=0;
                }
            }
            //Seat nomor 7
            else if(x >= 490 && x <= 530 && y >= 630 && y <= 730){
                if(ditekan[6]==0){
                    mCanvas.drawRect(240,330,290,380,seatDipilih);
                    mCanvas.drawRect(241,331,289,379,strokeSeatDipilih);
                    mCanvas.drawText("7",260,360,seatText);
                    ditekan[6]=1;
                }else{
                    mCanvas.drawRect(240,330,290,380,seatKosong);
                    mCanvas.drawRect(241,331,289,379,strokeSeatKosong);
                    mCanvas.drawText("7",260,360,seatText);
                    ditekan[6]=0;
                }
            }
            //Seat nomor 8
            else if(x >= 140 && x <= 190 && y >= 780 && y <= 900){
                if(ditekan[7]==0){
                    mCanvas.drawRect(60,420,110,470,seatDipilih);
                    mCanvas.drawRect(61,421,109,469,strokeSeatDipilih);
                    mCanvas.drawText("8",80,450,seatText);
                    ditekan[7]=1;
                }else{
                    mCanvas.drawRect(60,420,110,470,seatKosong);
                    mCanvas.drawRect(61,421,109,469,strokeSeatKosong);
                    mCanvas.drawText("8",80,450,seatText);
                    ditekan[7]=0;
                }
            }
            //Seat nomor 9
            else if(x >= 320 && x <= 350 && y >= 800 && y <= 900){
                if(ditekan[8]==0){
                    mCanvas.drawRect(150,420,200,470,seatDipilih);
                    mCanvas.drawRect(151,421,199,469,strokeSeatDipilih);
                    mCanvas.drawText("9",170,450,seatText);
                    ditekan[8]=1;
                }else{
                    mCanvas.drawRect(150,420,200,470,seatKosong);
                    mCanvas.drawRect(151,421,199,469,strokeSeatKosong);
                    mCanvas.drawText("9",170,450,seatText);
                    ditekan[8]=0;
                }
            }
            //Seat nomor 10
            else if(x >= 480 && x <= 520 && y >= 810 && y <= 910){
                if(ditekan[9]==0){
                    mCanvas.drawRect(240,420,290,470,seatDipilih);
                    mCanvas.drawRect(241,421,289,469,strokeSeatDipilih);
                    mCanvas.drawText("10",254,450,seatText);
                    ditekan[9]=1;
                }else{
                    mCanvas.drawRect(240,420,290,470,seatKosong);
                    mCanvas.drawRect(241,421,289,469,strokeSeatKosong);
                    mCanvas.drawText("10",254,450,seatText);
                    ditekan[9]=0;
                }
            }
            binding.ivSeat.invalidate();
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
            return true;
        }

        @Override
        public void onLongPress(MotionEvent motionEvent){

        }
    }
}
