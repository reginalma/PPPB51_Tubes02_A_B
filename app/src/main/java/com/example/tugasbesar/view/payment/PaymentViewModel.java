package com.example.tugasbesar.view.payment;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.android.volley.VolleyError;
import com.example.tugasbesar.service.APICallback;
import com.example.tugasbesar.service.APIService;
import com.example.tugasbesar.service.model.NewOrderRequestModel;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentViewModel extends AndroidViewModel implements APICallback {
    private final Context context;
    private final APIService service;
    private SharedPreferences sp;
    public PaymentViewModel(@NonNull Application application) {
        super(application);
        this.context = application;
        this.sp = application.getSharedPreferences("com.example.tugasbesar.sharedprefs", Context.MODE_PRIVATE);
        this.service = new APIService(application);
    }

    public void addNewOrders(NewOrderRequestModel requestModel) {
        NewOrderRequestModel newOrderRequestModel = new NewOrderRequestModel("2e3e6d64-6285-11ec-9868-eb31a4b04b67", "4, 7");
        service.newOrderTask(sp.getString("login_token", ""), newOrderRequestModel, this);
    }

    @Override
    public void onSuccess(JSONObject response) {
        try {
            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFail(VolleyError err) {
        JSONObject error = null;
        try {
            error = new JSONObject(new String(err.networkResponse.data));
            Log.d("RESPONSE-ERROR", error.getString("errcode"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(context, "Gagal memesan", Toast.LENGTH_SHORT).show();
    }
}
