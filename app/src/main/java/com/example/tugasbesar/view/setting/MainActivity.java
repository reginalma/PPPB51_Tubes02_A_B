package com.example.tugasbesar.view.setting;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentResultListener;
import androidx.fragment.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toolbar;
import com.example.tugasbesar.R;
import com.example.tugasbesar.SideBar;
import com.example.tugasbesar.databinding.ActivityMainBinding;
import com.example.tugasbesar.view.history.HistoryFragment;
import com.example.tugasbesar.view.home.HomeFragment;
import com.example.tugasbesar.view.login.LoginFragment;
import com.example.tugasbesar.view.order.OrderFragment;
import com.example.tugasbesar.view.seat.SeatLargeFragment;
import com.example.tugasbesar.view.seat.SeatSmallFragment;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    FragmentManager fragmentManager;
    DrawerLayout drawer;
    SideBar sideBar;
    LoginFragment halLogin;
    HomeFragment halHome;
    OrderFragment halOrder;
    SeatLargeFragment halSeatBesar;
    SeatSmallFragment halSeatKecil;
    com.example.tugasbesar.PaymentFragment halPayment;
    HistoryFragment halHistory;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding=ActivityMainBinding.inflate(this.getLayoutInflater());
        setContentView(binding.getRoot());

        this.halLogin = LoginFragment.newInstance();
        this.halHome = HomeFragment.newInstance();
        this.halOrder = OrderFragment.newInstance();
        this.halSeatBesar = SeatLargeFragment.newInstance();
        this.halSeatKecil = SeatSmallFragment.newInstance();
        this.halPayment = com.example.tugasbesar.PaymentFragment.newInstance();
        this.halHistory = HistoryFragment.newInstance();

        this.fragmentManager=this.getSupportFragmentManager();

        FragmentTransaction ft = this.fragmentManager.beginTransaction();

        ft.add(R.id.fragment_container, this.halLogin)
                .addToBackStack(null)
                .commit();

        this.getSupportFragmentManager().setFragmentResultListener(
                "changePage", this, new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                        int page = result.getInt("page");
                        changePage(page);
                    }
                }
        );
    }

    public void changePage(int page){
        FragmentTransaction ft = this.fragmentManager.beginTransaction();
        if (page == 3){
            if(this.halLogin.isAdded()){
                ft.show(this.halLogin);
            }
            else{
                ft.add(R.id.fragment_container, this.halLogin).addToBackStack(null);
            }
            if(this.halHome.isAdded()){
                ft.hide(this.halHome);
            }
            if(this.halOrder.isAdded()){
                ft.hide(this.halOrder);
            }
            if (this.halSeatBesar.isAdded()){
                ft.hide(this.halSeatBesar);
            }
            if (this.halSeatKecil.isAdded()){
                ft.hide(this.halSeatKecil);
            }
            if (this.halPayment.isAdded()){
                ft.hide(this.halPayment);
            }
            if (this.halHistory.isAdded()){
                ft.hide(this.halHistory);
            }
        }
        if (page == 1){
            if(this.halOrder.isAdded()){
                ft.show(this.halOrder);
            }
            else{
                ft.add(R.id.fragment_container,this.halOrder).addToBackStack(null);
            }
        }
        else if (page == 2){
            if (this.halHistory.isAdded()){
                ft.show(this.halHistory);
            }
            else{
                ft.add(R.id.fragment_container, this.halHistory).addToBackStack(null);
            }
        }
        else if (page == 0){
            if(this.halHome.isAdded()){
                ft.show(this.halHome);
            }
            else{
                ft.add(R.id.fragment_container, this.halHome);
            }
            if(this.halLogin.isAdded()){
                ft.hide(this.halLogin);
            }
            if(this.halOrder.isAdded()){
                ft.hide(this.halOrder);
            }
            if (this.halSeatBesar.isAdded()){
                ft.hide(this.halSeatBesar);
            }
            if (this.halSeatKecil.isAdded()){
                ft.hide(this.halSeatKecil);
            }
            if (this.halPayment.isAdded()){
                ft.hide(this.halPayment);
            }
            if (this.halHistory.isAdded()){
                ft.hide(this.halHistory);
            }
        }
        else if (page == 4){
            if (this.halSeatBesar.isAdded()){
                ft.show(this.halSeatBesar);
            }
            else{
                ft.add(R.id.fragment_container, this.halSeatBesar).addToBackStack(null);
            }
        }
        else if (page == 5){
            if (this.halPayment.isAdded()){
                ft.show(this.halPayment);
            }
            else{
                ft.add(R.id.fragment_container, this.halPayment).addToBackStack(null);
            }
        }
        else if (page == 6){
            if (this.halSeatKecil.isAdded()){
                ft.show(this.halSeatKecil);
            }
            else{
                ft.add(R.id.fragment_container, this.halSeatKecil).addToBackStack(null);
            }
        }
        ft.commit();
    }

    public void closeApplication(){
        this.moveTaskToBack(true);
        this.finish();
    }
}