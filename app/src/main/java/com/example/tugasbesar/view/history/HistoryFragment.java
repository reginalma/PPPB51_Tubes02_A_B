package com.example.tugasbesar.view.history;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tugasbesar.adapter.PemesananAdapter;
import com.example.tugasbesar.databinding.HistoryBinding;
import com.example.tugasbesar.view.history.HistoryViewModel;
import com.example.tugasbesar.view.order.OrderViewModel;

public class HistoryFragment extends Fragment{
    private HistoryBinding binding;
    private HistoryViewModel viewModel;
    private PemesananAdapter adapter;

    public static HistoryFragment newInstance(){
        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = HistoryBinding.inflate(inflater, container, false);
        return this.binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.adapter = new PemesananAdapter(this);
        this.binding.listPemesanan.setAdapter(adapter);

        this.viewModel = new ViewModelProvider(this).get(HistoryViewModel.class);
        this.viewModel.getHistory();
        this.viewModel.historyList.observe(requireActivity(), histories -> {
            adapter.add(histories);
        });
    }
}
