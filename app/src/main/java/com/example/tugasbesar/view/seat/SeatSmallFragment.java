package com.example.tugasbesar.view.seat;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import com.example.tugasbesar.R;
import com.example.tugasbesar.databinding.SeatSmallBinding;

public class SeatSmallFragment extends Fragment implements View.OnClickListener, View.OnTouchListener {
    SeatSmallBinding binding;
    GestureDetector mDetector;
    Bitmap mBitmap;
    Canvas mCanvas;
    Paint seatKosong;
    Paint seatDipilih;
    Paint strokeSeatKosong;
    Paint seatText;
    Paint strokeSeatDipilih;
    int[] ditekan=new int[7];

    public static SeatSmallFragment newInstance(){
        SeatSmallFragment fragment = new SeatSmallFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = SeatSmallBinding.inflate(inflater, container, false);
        this.startCanvas();
        this.mDetector = new GestureDetector(this.getContext(), new customGestureListener());
        this.binding.ivSeat.setOnTouchListener(this);
        this.binding.btnLanjut.setOnClickListener(this);
        return this.binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        Log.d("debug", "clicked!");
        if (v == this.binding.btnLanjut) {
            Bundle result = new Bundle();
            result.putInt("page", 5);
            this.getParentFragmentManager().setFragmentResult("changePage", result);
        }
    }

    public void startCanvas(){
        //Create bitmap
        this.mBitmap=Bitmap.createBitmap(350,350,Bitmap.Config.ARGB_8888);

        //Associate the bitmap to the ImageView
        this.binding.ivSeat.setImageBitmap(mBitmap);

        //Create a canvas with the bitmap
        //Inisialisasi dengan bitmap
        this.mCanvas=new Canvas(mBitmap);

        //Set canvas background
        int mColorBackground= ResourcesCompat.getColor(getResources(),
                R.color.white, null);
        mCanvas.drawColor(mColorBackground);

        //Color and style
        //Seat kosong
        this.seatKosong = new Paint();
        seatKosong.setColor(this.getResources().getColor(R.color.black));
        this.seatKosong.setStyle(Paint.Style.FILL);

        this.strokeSeatKosong = new Paint();
        strokeSeatKosong.setColor(this.getResources().getColor(R.color.gold));
        this.seatKosong.setStyle(Paint.Style.STROKE);
        this.strokeSeatKosong.setStrokeWidth(5);

        this.seatText = new Paint();
        seatText.setColor(this.getResources().getColor(R.color.black));
        this.seatText.setTextSize(20);

        //Seat dipilih
        seatDipilih = new Paint();
        seatDipilih.setColor(getResources().getColor(R.color.black));
        seatDipilih.setStyle(Paint.Style.FILL);

        strokeSeatDipilih = new Paint();
        strokeSeatDipilih.setColor(this.getResources().getColor(R.color.red));
        seatDipilih.setStyle(Paint.Style.STROKE);
        strokeSeatDipilih.setStrokeWidth(5);

        seatText = new Paint();
        seatText.setColor(this.getResources().getColor(R.color.black));
        seatText.setTextSize(20);

        //Seat nomor 1
        mCanvas.drawRect(60,60,110,110,seatKosong);
        mCanvas.drawRect(61,61,109,109,strokeSeatKosong);
        mCanvas.drawText("1",80,90,seatText);

        //Seat supir
        mCanvas.drawRect(240,60,290,110,seatKosong);
        mCanvas.drawRect(241,61,289,109,strokeSeatKosong);
        mCanvas.drawText("S",260,90,seatText);

        //Seat nomor 2
        mCanvas.drawRect(150,150,200,200,seatKosong);
        mCanvas.drawRect(151,151,199,199,strokeSeatKosong);
        mCanvas.drawText("2",170,180,seatText);

        //Seat nomor 3
        mCanvas.drawRect(240,150,290,200,seatKosong);
        mCanvas.drawRect(241,151,289,199,strokeSeatKosong);
        mCanvas.drawText("3",260,180,seatText);

        //Seat nomor 4
        mCanvas.drawRect(60,240,110,290,seatKosong);
        mCanvas.drawRect(61,241,109,289,strokeSeatKosong);
        mCanvas.drawText("4",80,270,seatText);

        //Seat nomor 5
        mCanvas.drawRect(150,240,200,290,seatKosong);
        mCanvas.drawRect(151,241,199,289,strokeSeatKosong);
        mCanvas.drawText("5",170,270,seatText);

        //Seat nomor 6
        mCanvas.drawRect(240,240,290,290,seatKosong);
        mCanvas.drawRect(241,241,289,289,strokeSeatKosong);
        mCanvas.drawText("6",260,270,seatText);
        Log.d("test","test");
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        this.mDetector.onTouchEvent(motionEvent);
        return true;
    }

    private class customGestureListener extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onDown(MotionEvent motionEvent){
            Log.d("X=",String.valueOf(motionEvent.getX()));
            Log.d("Y=",String.valueOf(motionEvent.getY()));

            float x = motionEvent.getX();
            float y = motionEvent.getY();

            //Seat nomor 1
            if(x >= 150 && x <= 190 && y >= 150 && y <= 190){
                if(ditekan[0]==0){
                    mCanvas.drawRect(60,60,110,110,seatDipilih);
                    mCanvas.drawRect(61,61,109,109,strokeSeatDipilih);
                    mCanvas.drawText("1",80,90,seatText);
                    ditekan[0]=1;
                }else{
                    mCanvas.drawRect(60,60,110,110,seatKosong);
                    mCanvas.drawRect(61,61,109,109,strokeSeatKosong);
                    mCanvas.drawText("1",80,90,seatText);
                    ditekan[0]=0;
                }
            }
            //Seat nomor 2
            else if(x >= 300 && x <= 350 && y >= 300 && y <= 350){
                if(ditekan[1]==0){
                    mCanvas.drawRect(150,150,200,200,seatDipilih);
                    mCanvas.drawRect(151,151,199,199,strokeSeatDipilih);
                    mCanvas.drawText("2",170,180,seatText);
                    ditekan[1]=1;
                }else{
                    mCanvas.drawRect(150,150,200,200,seatKosong);
                    mCanvas.drawRect(151,151,199,199,strokeSeatKosong);
                    mCanvas.drawText("2",170,180,seatText);
                    ditekan[1]=0;
                }
            }
            //Seat nomor 3
            else if(x >= 480 && x <= 530 && y >= 290 && y <= 390){
                if(ditekan[2]==0){
                    mCanvas.drawRect(240,150,290,200,seatDipilih);
                    mCanvas.drawRect(241,151,289,199,strokeSeatDipilih);
                    mCanvas.drawText("3",260,180,seatText);
                    ditekan[2]=1;
                }else{
                    mCanvas.drawRect(240,150,290,200,seatKosong);
                    mCanvas.drawRect(241,151,289,199,strokeSeatKosong);
                    mCanvas.drawText("3",260,180,seatText);
                    ditekan[2]=0;
                }
            }
            //Seat nomor 4
            else if(x >= 140 && x <= 180 && y >= 490 && y <= 550){
                if(ditekan[3]==0){
                    mCanvas.drawRect(60,240,110,290,seatDipilih);
                    mCanvas.drawRect(60,240,110,290,strokeSeatDipilih);
                    mCanvas.drawText("4",80,270,seatText);
                    ditekan[3]=1;
                }else{
                    mCanvas.drawRect(60,240,110,290,seatKosong);
                    mCanvas.drawRect(60,240,110,290,strokeSeatKosong);
                    mCanvas.drawText("4",80,270,seatText);
                    ditekan[3]=0;
                }
            }
            //Seat nomor 5
            else if(x >= 290 && x <= 330 && y >= 450 && y <= 550){
                if(ditekan[4]==0){
                    mCanvas.drawRect(150,240,200,290,seatDipilih);
                    mCanvas.drawRect(151,241,199,289,strokeSeatDipilih);
                    mCanvas.drawText("5",170,270,seatText);
                    ditekan[4]=1;
                }else{
                    mCanvas.drawRect(150,240,200,290,seatKosong);
                    mCanvas.drawRect(151,241,199,289,strokeSeatKosong);
                    mCanvas.drawText("5",170,270,seatText);
                    ditekan[4]=0;
                }
            }
            //Seat nomor 6
            else if(x >= 490 && x <= 550 && y >= 490 && y <= 560){
                if(ditekan[5]==0){
                    mCanvas.drawRect(240,240,290,290,seatDipilih);
                    mCanvas.drawRect(241,241,289,289,strokeSeatDipilih);
                    mCanvas.drawText("6",260,270,seatText);
                    ditekan[5]=1;
                }else{
                    mCanvas.drawRect(240,240,290,290,seatKosong);
                    mCanvas.drawRect(241,241,289,289,strokeSeatKosong);
                    mCanvas.drawText("6",260,270,seatText);
                    ditekan[5]=0;
                }
            }
            binding.ivSeat.invalidate();
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
            return true;
        }

        @Override
        public void onLongPress(MotionEvent motionEvent){

        }
    }
}
