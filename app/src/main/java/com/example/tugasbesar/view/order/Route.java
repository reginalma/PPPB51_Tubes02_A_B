package com.example.tugasbesar.view.order;

public class Route {
    String source;
    String destination;
    int fee;

    public Route(String source, String destination, int fee) {
        this.source = source;
        this.destination = destination;
        this.fee = fee;
    }
}
