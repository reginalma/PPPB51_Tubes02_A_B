package com.example.tugasbesar.view.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tugasbesar.databinding.LoginBinding;

public class LoginFragment extends Fragment implements View.OnClickListener {
    private LoginBinding binding;
    private LoginViewModel viewModel;

    public static LoginFragment newInstance(){
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = LoginBinding.inflate(inflater, container, false);
        return this.binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        this.binding.btnLogin.setOnClickListener(this);
        this.viewModel = new ViewModelProvider(this).get(LoginViewModel.class);

        this.viewModel.isLogin.observe(requireActivity(), isLogin -> {
            if(isLogin){
                Bundle result = new Bundle();
                result.putInt("page", 0);
                this.getParentFragmentManager().setFragmentResult("changePage", result);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == this.binding.btnLogin) {
            String username = this.binding.username.getText().toString().trim();
            String password = this.binding.password.getText().toString().trim();

            if(!username.isEmpty() || !password.isEmpty()){
                this.viewModel.login(username, password);
            }else{
                Toast.makeText(getActivity(), "Silahkan mengisi form untuk login", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

