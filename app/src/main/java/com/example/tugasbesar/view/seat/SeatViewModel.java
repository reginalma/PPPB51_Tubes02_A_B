package com.example.tugasbesar.view.seat;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import com.android.volley.VolleyError;
import com.example.tugasbesar.service.APICallback;
import com.example.tugasbesar.service.APIService;
import com.example.tugasbesar.service.model.CourseRequestModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SeatViewModel extends AndroidViewModel implements APICallback {
    private final Context context;
    private final APIService service;
    private SharedPreferences sp;
    public MutableLiveData<Seat> takenSeats;

    public SeatViewModel(@NonNull Application application) {
        super(application);
        this.context = application;
        this.sp = application.getSharedPreferences("com.example.tugasbesar.sharedprefs", Context.MODE_PRIVATE);
        this.service = new APIService(application);
        this.takenSeats = new MutableLiveData<>();
    }

    public void getCourse(CourseRequestModel course) {
        CourseRequestModel courses = new CourseRequestModel(
                "Jakarta",
                "Bandung",
                "Large",
                "18-03-2022",
                "03"
        );
        service.courseTask(sp.getString("login_token", ""), courses, this);
    }

    @Override
    public void onSuccess(JSONObject response) {
        JSONArray payloads = null;
        Seat seat = new Seat();
        try {
            payloads = response.getJSONArray("payload");
            JSONObject payload = payloads.getJSONObject(0);
            JSONArray jsonSeats = payload.getJSONArray("seats");
            int[] seats = new int[jsonSeats.length()];
            for (int i = 0; i < jsonSeats.length(); i++) {
                seats[i] = jsonSeats.getInt(i);
            }
            seat = new Seat(seats, payload.getString("course_id"));
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context, "Gagal mendapatkan course", Toast.LENGTH_SHORT).show();
        }
        takenSeats.setValue(seat);
    }

    @Override
    public void onFail(VolleyError err) {
        JSONObject error = null;
        try {
            error = new JSONObject(new String(err.networkResponse.data));
            Log.d("RESPONSE-ERROR", error.getString("errcode"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(context, "Gagal mendapatkan seat", Toast.LENGTH_SHORT).show();
    }
}
