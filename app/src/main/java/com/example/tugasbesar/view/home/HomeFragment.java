package com.example.tugasbesar.view.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.Glide;
import com.example.tugasbesar.databinding.HomeBinding;

public class HomeFragment extends Fragment implements View.OnClickListener{
    HomeBinding binding;
    ImageView gif;

    public static HomeFragment newInstance(){
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = HomeBinding.inflate(inflater, container, false);

        //library glide untuk menampilkan gif welcome
        gif = binding.gif;
        Glide.with(this)
                .load("https://media.giphy.com/media/xUPGGDNsLvqsBOhuU0/giphy.gif")
                .into(gif);

        this.binding.btnPesanSekarang.setOnClickListener(this);

        return this.binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        Log.d("debug", "clicked!");
        if (v == this.binding.btnPesanSekarang) {
            Bundle result = new Bundle();
            result.putInt("page", 1);
            this.getParentFragmentManager().setFragmentResult("changePage", result);
        }
    }
}
