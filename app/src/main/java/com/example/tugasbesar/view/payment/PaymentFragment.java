package com.example.tugasbesar;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tugasbesar.databinding.PaymentBinding;
import com.example.tugasbesar.service.model.NewOrderRequestModel;
import com.example.tugasbesar.view.order.OrderViewModel;
import com.example.tugasbesar.view.payment.PaymentViewModel;

public class PaymentFragment extends Fragment implements View.OnClickListener{
    private PaymentBinding binding;
    private PaymentViewModel viewModel;

    public static PaymentFragment newInstance(){
        PaymentFragment fragment = new PaymentFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = PaymentBinding.inflate(inflater, container, false);
        this.binding.btnPesan.setOnClickListener(this);
        return this.binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.viewModel = new ViewModelProvider(this).get(PaymentViewModel.class);
    }

    @Override
    public void onClick(View v) {
        Log.d("debug", "clicked!");
        if (v == this.binding.btnPesan) {
//        NewOrderRequestModel diisi course id sama seat yang mau dipesen
            this.viewModel.addNewOrders(new NewOrderRequestModel("", ""));
            Bundle result = new Bundle();
            result.putInt("page", 2);
            this.getParentFragmentManager().setFragmentResult("changePage", result);
        }
    }
}
