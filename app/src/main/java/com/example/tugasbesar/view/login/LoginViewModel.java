package com.example.tugasbesar.view.login;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import com.android.volley.VolleyError;
import com.example.tugasbesar.service.APICallback;
import com.example.tugasbesar.service.APIService;
import com.example.tugasbesar.service.model.LoginRequestModel;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginViewModel extends AndroidViewModel implements APICallback {
    public MutableLiveData<Boolean> isLogin;
    private final Context context;
    private final APIService service;
    private SharedPreferences sp;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        this.isLogin = new MutableLiveData<>(false);
        this.context = application;
        this.sp = application.getSharedPreferences("com.example.tugasbesar.sharedprefs", Context.MODE_PRIVATE);
        this.service = new APIService(application);
    }

    public void login(String username, String passsword){
        service.loginTask(new LoginRequestModel(username, passsword), this);
    }

    @Override
    public void onSuccess(JSONObject response) {
        SharedPreferences.Editor edit = sp.edit();
        try {
            edit.putString("login_token", response.getString("token"));
            edit.apply();
            Toast.makeText(context, "Login berhasil", Toast.LENGTH_SHORT).show();
            this.isLogin.setValue(true);
            Log.d("login_token", response.getString("token"));
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context, "Gagal mengambil token", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFail(VolleyError err) {
        JSONObject error = null;
        try {
            error = new JSONObject(new String(err.networkResponse.data));
            Log.d("RESPONSE-ERROR", error.getString("errcode"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(context, "Login gagal dilakukan!", Toast.LENGTH_SHORT).show();
    }
}