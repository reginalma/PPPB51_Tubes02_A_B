package com.example.tugasbesar.view.order;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import com.android.volley.VolleyError;
import com.example.tugasbesar.service.APICallback;
import com.example.tugasbesar.service.APIService;
import com.example.tugasbesar.service.model.CourseRequestModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class OrderViewModel extends AndroidViewModel implements APICallback {
    private final Context context;
    private final APIService service;
    private SharedPreferences sp;
    public MutableLiveData<List<Route>> routeList;

    public OrderViewModel(@NonNull Application application) {
        super(application);
        this.context = application;
        this.sp = application.getSharedPreferences("com.example.tugasbesar.sharedprefs", Context.MODE_PRIVATE);
        this.service = new APIService(application);
        this.routeList = new MutableLiveData<>();
    }

    public void getRoute() {
        service.routeTask(sp.getString("login_token", ""), this);
    }

    public void getCourse(CourseRequestModel course){
        service.courseTask(sp.getString("login_token", ""), course, this);
    }

    @Override
    public void onSuccess(JSONObject response) {
        List<Route> routeList = new ArrayList<>();
        JSONArray payloads = null;
        try {
            payloads = response.getJSONArray("payload");
            for (int i = 0; i < payloads.length(); i++) {
                JSONObject payload = payloads.getJSONObject(i);
                routeList.add(new Route(
                        payload.getString("source"),
                        payload.getString("destination"),
                        Integer.parseInt(payload.getString("fee"))
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.routeList.setValue(routeList);
    }

    @Override
    public void onFail(VolleyError err) {
        JSONObject error = null;
        try {
            error = new JSONObject(new String(err.networkResponse.data));
            Log.d("RESPONSE-ERROR", error.getString("errcode"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(context, "Gagal memesan", Toast.LENGTH_SHORT).show();
    }
}
