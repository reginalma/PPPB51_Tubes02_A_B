package com.example.tugasbesar.view.order;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.example.tugasbesar.databinding.OrderBinding;
import com.example.tugasbesar.service.model.CourseRequestModel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class OrderFragment extends Fragment implements View.OnClickListener{
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    SimpleDateFormat dateFormat;
    int hour, minute;
    private OrderBinding binding;
    private OrderViewModel viewModel;
    String token;

    public static OrderFragment newInstance(){
        OrderFragment fragment = new OrderFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.binding = OrderBinding.inflate(inflater, container, false);

        //Dropdown
        //Kota Asal
        Spinner kotaAsal = (Spinner) binding.spinnerKotaAsal;
        String [] kotaAsalArray = {"Bandung","Bekasi","Cikarang","Jakarta"};
        ArrayAdapter<String> kotaAsalAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item,kotaAsalArray);
        kotaAsalAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        kotaAsal.setAdapter(kotaAsalAdapter);

        //Kota Tujuan
        Spinner kotaTujuan = (Spinner) binding.spinnerKotaTujuan;
        String [] kotaTujuanArray ={"Bandung","Bekasi","Cikarang","Jakarta"};
        ArrayAdapter<String> kotaTujuanAdapter =new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item,kotaTujuanArray);
        kotaTujuanAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        kotaTujuan.setAdapter(kotaTujuanAdapter);

        //Jenis Kendaraan
        Spinner jenisKendaraan = (Spinner) binding.spinnerJenisKendaraan;
        String [] kendaraanArray = {"Large","Small"};
        ArrayAdapter<String> kendaraanAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item,kendaraanArray);
        kendaraanAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        jenisKendaraan.setAdapter(kendaraanAdapter);

        return this.binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.binding.btnCari.setOnClickListener(this);

        this.binding.btnTanggal.setOnClickListener(this);
        this.dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        this.binding.btnJam.setOnClickListener(this);

        this.viewModel = new ViewModelProvider(this).get(OrderViewModel.class);
        this.viewModel.getRoute();
        this.viewModel.routeList.observe(requireActivity(), routes -> {
            Toast.makeText(requireActivity(), "Rute berhasil didapatkan", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public void onClick(View v) {
        Log.d("debug", "clicked!");
        if(v == this.binding.btnCari) {
            String hour = binding.tvJam.getText().toString();
            String date = binding.tvTanggal.getText().toString();
            String source = binding.spinnerKotaAsal.getSelectedItem().toString();
            String destination = binding.spinnerKotaAsal.getSelectedItem().toString();
            String vehicle = binding.spinnerKotaAsal.getSelectedItem().toString();

            if(!source.isEmpty() && !destination.isEmpty() && !date.isEmpty() && !hour.isEmpty() && !vehicle.isEmpty() && this.binding.spinnerJenisKendaraan.getSelectedItem().toString() == "Large") {
                Bundle result =new Bundle();
                result.putInt("page",4); //halaman seat untuk jenis kendaraan large

                result.putString("source", this.binding.spinnerKotaAsal.getSelectedItem().toString());
                result.putString("destination", this.binding.spinnerKotaTujuan.getSelectedItem().toString());
                result.putString("vehicle", this.binding.spinnerJenisKendaraan.getSelectedItem().toString());
                result.putString("date", binding.tvTanggal.getText().toString());
                result.putString("hour",binding.tvJam.getText().toString());

                hour = binding.tvJam.getText().toString().substring(0,2);

//                CourseRequestModel course = new CourseRequestModel(source, destination, vehicle, date, hour);
//                this.viewModel.getCourse(course);

                this.getParentFragmentManager().setFragmentResult("source",result);
                this.getParentFragmentManager().setFragmentResult("destination",result);
                this.getParentFragmentManager().setFragmentResult("vehicle",result);
                this.getParentFragmentManager().setFragmentResult("date",result);
                this.getParentFragmentManager().setFragmentResult("time",result);
                this.getParentFragmentManager().setFragmentResult("tokenOrder",result);
                this.getParentFragmentManager().setFragmentResult("changePage",result);
            }
            else if(!source.isEmpty() && !destination.isEmpty() && !date.isEmpty() && !hour.isEmpty() && !vehicle.isEmpty() && this.binding.spinnerJenisKendaraan.getSelectedItem().toString() == "Small") {
                Bundle result =new Bundle();
                result.putInt("page",6); //halaman seat untuk jenis kendaraan small

                result.putString("source", this.binding.spinnerKotaAsal.getSelectedItem().toString());
                result.putString("destination", this.binding.spinnerKotaTujuan.getSelectedItem().toString());
                result.putString("vehicle", this.binding.spinnerJenisKendaraan.getSelectedItem().toString());
                result.putString("date", binding.tvTanggal.getText().toString());
                result.putString("hour",binding.tvJam.getText().toString());

                hour = binding.tvJam.getText().toString().substring(0,2);

//                CourseRequestModel course = new CourseRequestModel(source, destination, vehicle, date, hour);
//                this.viewModel.getCourse(course);

                this.getParentFragmentManager().setFragmentResult("source",result);
                this.getParentFragmentManager().setFragmentResult("destination",result);
                this.getParentFragmentManager().setFragmentResult("vehicle",result);
                this.getParentFragmentManager().setFragmentResult("tokenOrder",result);
                this.getParentFragmentManager().setFragmentResult("changePage",result);
            }else{
                Toast.makeText(getActivity(), "Silahkan mengisi form untuk membuat pesanan", Toast.LENGTH_SHORT).show();
            }
        }
        else if(v == this.binding.btnTanggal) {
            date();
        }
        else if(v == this.binding.btnJam) {
            time();
        }
    }

    public void time() {
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            TextView tvTime = binding.tvJam;
            @Override
            public void onTimeSet(TimePicker timePicker, int selectHour, int selectMinute) {
                hour = selectHour;
                minute = selectMinute;
                tvTime.setText(String.format(Locale.getDefault(), "%02d:%02d", hour, minute));
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), onTimeSetListener, hour, minute, true);
        timePickerDialog.show();
    }

    public void date() {
        Calendar calendar = Calendar.getInstance();
        TextView tvDate = binding.tvTanggal;
        this.datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(i, i1, i2);
                tvDate.setText(dateFormat.format(newDate.getTime()));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
}