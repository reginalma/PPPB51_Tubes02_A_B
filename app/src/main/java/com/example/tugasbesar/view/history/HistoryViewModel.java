package com.example.tugasbesar.view.history;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.VolleyError;
import com.example.tugasbesar.service.APICallback;
import com.example.tugasbesar.service.APIService;
import com.example.tugasbesar.view.order.Route;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HistoryViewModel extends AndroidViewModel implements APICallback {
    private final Context context;
    private final APIService service;
    private SharedPreferences sp;
    public MutableLiveData<List<History>> historyList;

    public HistoryViewModel(@NonNull Application application) {
        super(application);
        this.context = application;
        this.sp = application.getSharedPreferences("com.example.tugasbesar.sharedprefs", Context.MODE_PRIVATE);
        this.service = new APIService(application);
        this.historyList = new MutableLiveData<>();
    }

    public void getHistory() {
        service.historyTask(sp.getString("login_token", ""), this);
    }

    @Override
    public void onSuccess(JSONObject response) {
        List<History> historyList = new ArrayList<>();
        JSONArray payloads = null;
        try {
            payloads = response.getJSONArray("payload");
            for (int i = 0; i < payloads.length(); i++) {
                JSONObject payload = payloads.getJSONObject(i);
                historyList.add(new History(
                        payload.getString("source"),
                        payload.getString("destination"),
                        payload.getString("course_datetime")
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.historyList.setValue(historyList);
    }

    @Override
    public void onFail(VolleyError err) {
        JSONObject error = null;
        try {
            error = new JSONObject(new String(err.networkResponse.data));
            Log.d("RESPONSE-ERROR", error.getString("errcode"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(context, "Gagal memesan", Toast.LENGTH_SHORT).show();
    }
}
