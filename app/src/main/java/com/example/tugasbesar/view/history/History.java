package com.example.tugasbesar.view.history;

public class History {
    public String source;
    public String destination;
    public String course_datetime;

    public History(String source, String destination, String course_datetime) {
        this.source = source;
        this.destination = destination;
        this.course_datetime = course_datetime;
    }
}
